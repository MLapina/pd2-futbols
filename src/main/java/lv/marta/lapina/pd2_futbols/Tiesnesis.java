/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.marta.lapina.pd2_futbols;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Marta
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Tiesnesis extends Persona {
    @JsonProperty("Vards")
    private boolean galvenais = false;

    public Tiesnesis (String vards, String uzvards, boolean galvenais){
        super.setVards(vards);
        super.setUzvards(uzvards);
        this.galvenais = galvenais;
    }

    @JsonCreator
    public Tiesnesis() {
    }

    @Override
    public String toString() {
        return "Tiesnesis{ " +
                "vards:" + getVards() +
                ",uzvards:" + getUzvards() +
                "}";
    }
}
