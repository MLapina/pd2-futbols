package lv.marta.lapina.pd2_futbols;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Komanda {
    @JsonProperty("Nosaukums")
    private String nosaukums;
    @JsonProperty("Speletaji")
    private Speletaji speletaji;
    @JsonProperty("Pamatsastavs")
    private Speletaji pamatsastavs;
    @JsonProperty("Varti")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Varti varti;
    @JsonProperty("Mainas")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Mainas mainas;
    @JsonProperty("Sodi")
    private Sodi sodi;

    @JsonCreator
    public Komanda() {
    }

    public Komanda(String nosaukums, Speletaji speletaji, Speletaji pamatsastavs, Varti varti, Mainas mainas, Sodi sodi) {
        this.nosaukums = nosaukums;
        this.speletaji = speletaji;
        this.pamatsastavs = pamatsastavs;
        this.varti = varti;
        this.mainas = mainas;
        this.sodi = sodi;
    }

    public String getNosaukums() {
        return nosaukums;
    }

    public void setNosaukums(String nosaukums) {
        this.nosaukums = nosaukums;
    }

    public Speletaji getSpeletaji() {
        return speletaji;
    }

    public void setSpeletaji(Speletaji speletaji) {
        this.speletaji = speletaji;
    }

    public Speletaji getPamatsastavs() {
        return pamatsastavs;
    }

    public void setPamatsastavs(Speletaji pamatsastavs) {
        this.pamatsastavs = pamatsastavs;
    }

    public Varti getVarti() {
        return varti;
    }

    public void setVarti(Varti varti) {
        this.varti = varti;
    }

    public Mainas getMainas() {
        return mainas;
    }

    public void setMainas(Mainas mainas) {
        this.mainas = mainas;
    }

    public Sodi getSodi() {
        return sodi;
    }

    public void setSodi(Sodi sodi) {
        this.sodi = sodi;
    }

}
