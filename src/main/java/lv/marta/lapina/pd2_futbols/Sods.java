/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.marta.lapina.pd2_futbols;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Marta
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Sods {

    @JsonProperty("Laiks")
    private String laiks;
    @JsonProperty("Nr")
    private int parkapejs;

    public Sods (String laiks, int parkapejs){
        this.laiks = laiks;;
        this.parkapejs = parkapejs;
    }

    @JsonCreator
    public Sods() {
    }

    public String getLaiks() {
        return laiks;
    }

    public void setLaiks(String laiks) {
        this.laiks = laiks;
    }

    public int getParkapejs() {
        return parkapejs;
    }

    public void setParkapejs(int parkapejs) {
        this.parkapejs = parkapejs;
    }
}