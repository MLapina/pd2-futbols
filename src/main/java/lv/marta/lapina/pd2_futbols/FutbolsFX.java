package lv.marta.lapina.pd2_futbols;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FutbolsFX
        extends Application {

    private Futbols fb;
    private Text actionStatus;
    private Stage savedStage;
    private static final String titleTxt = "Futbola statistika";

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

        fb = new Futbols();

        primaryStage.setTitle(titleTxt);

        //Data selection button
        Button fileSelection = new Button("Izvēlieties datu failus...");
        fileSelection.setOnAction(new MultipleFcButtonListener());
        HBox fileSelectionHB = new HBox(10);
        fileSelectionHB.setAlignment(Pos.CENTER);
        fileSelectionHB.getChildren().addAll(fileSelection);

        //Statistics button
        Button stats = new Button("Ģenerēt statistiku...");
        stats.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                fb.connectToDB();
//                fb.getMatchesFromDb();
                //makeChart();
                makeTable();
                //Stage stage = new Stage();
                //Fill stage with content
                //  stage.show();
            }
        });
        HBox statsHB = new HBox(10);
        statsHB.setAlignment(Pos.CENTER);
        statsHB.getChildren().addAll(stats);

        //Exit button
        Button exit = new Button("Beigt darbu");
        exit.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                Platform.exit();
                System.exit(0);
            }
        });
        HBox exitHB = new HBox(10);
        exitHB.setAlignment(Pos.CENTER);
        exitHB.getChildren().addAll(exit);

        // Status message
        actionStatus = new Text();
        actionStatus.setFont(Font.font("Calibri", FontWeight.NORMAL, 20));
        actionStatus.setFill(Color.GREEN);

        VBox vb = new VBox(30);
        vb.setPadding(new Insets(25, 25, 25, 25));
        ;
        vb.getChildren().addAll(fileSelectionHB, statsHB, actionStatus, exitHB);

        Scene scene = new Scene(vb, 600, 400);
        primaryStage.setScene(scene);
        primaryStage.show();

        savedStage = primaryStage;
    }

    private class MultipleFcButtonListener implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent e) {
            showMultipleFileChooser();
        }
    }

    private void showMultipleFileChooser() {

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Datu izvēle");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        fileChooser.getExtensionFilters().addAll(
                new ExtensionFilter("JSON Files", "*.json"));
        List<File> selectedFiles = fileChooser.showOpenMultipleDialog(savedStage);

        if (selectedFiles != null) {
            fb.connectToDB();
            actionStatus.setText(saveFilesToDb(fb, selectedFiles));
        } else {
            actionStatus.setText("Datu izvēle atcelta.");
        }
    }

    private String saveFilesToDb(Futbols f, List<File> lf) {
        StringBuilder message = new StringBuilder();
        for (File file : lf) {
            //print out which files were added and which already exist
            if (f.postFileToDB(file) == 0) {
                message.append("Saglabāts fails: " + file.getName() + "\n");
            } else {
                message.append("Saturs no faila " + file.getName() + " jau eksistē sistēmā! \n");
            }
        }
        return message.toString();
    }

    private void makeChart() {
        Stage stage = new Stage();
        stage.setTitle("Statistics");
        Map<String, ArrayList<Spele>> apmekletajuStatistika = fb.getApmekletajuStatistika();
        stage.setTitle("Apmeklējuma statistika");
        //defining the axes
        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("Datums");
        yAxis.setLabel("Apmeklētāju skaits");
        //creating the chart
        final LineChart<String, Number> lineChart =
                new LineChart<String, Number>(xAxis, yAxis);

        lineChart.setTitle("Apmeklējuma statistika");
        //defining a series
        List<Series> seriesSet = null;
        Scene scene = new Scene(lineChart, 800, 600);
        for (String stadions : apmekletajuStatistika.keySet()) {
            Series<String, Number> series = new Series<>();
            series.setName(stadions.toString());
            ArrayList<Spele> speles = apmekletajuStatistika.get(stadions);
            for (int i = 0; i < speles.size(); i++) {
                series.getData().add(new XYChart.Data(speles.get(i).getLaiks().toString(), speles.get(i).getSkatitaji()));
            }
            lineChart.getData().add(series);
        };

        stage.setScene(scene);
        stage.show();
    }



    private void makeTable() {
        TableView table = new TableView();
        table.setPrefWidth(1150d);
        table.setPrefHeight(290d);
        Stage stage = new Stage();
        Scene scene = new Scene(new Group());
        stage.setTitle("Komandu statistika");
        stage.setWidth(1200);
        stage.setHeight(400);

        final Label label = new Label("Komandu statistika");
        label.setFont(new Font("Arial", 20));

        TableColumn reitingColumn = new TableColumn("Reitings");
        reitingColumn.setCellValueFactory(new PropertyValueFactory<KomandasStatistika, String>("reitings"));
        TableColumn nosaukumaColumn = new TableColumn("Nosaukums");
        nosaukumaColumn.setCellValueFactory(new PropertyValueFactory<KomandasStatistika, String>("nosaukums"));
        TableColumn iegutoPunktuColumn = new TableColumn("Iegūtie punkti");
        iegutoPunktuColumn.setCellValueFactory(new PropertyValueFactory<KomandasStatistika, String>("iegutiePunkti"));
        TableColumn pamatlaikaUzvarasColumn = new TableColumn("Pamatlaika uzvaras");
        pamatlaikaUzvarasColumn.setCellValueFactory(new PropertyValueFactory<KomandasStatistika, String>("pamatlaikaUzvaras"));
        TableColumn pamatlaikaZaudejumiColumn = new TableColumn("Pamatlaika zaudējumi");
        pamatlaikaZaudejumiColumn.setCellValueFactory(new PropertyValueFactory<KomandasStatistika, String>("pamatlaikaZaudejumi"));
        TableColumn papildlaikaUzvarasColumn = new TableColumn("Papildlaika uzvaras");
        papildlaikaUzvarasColumn.setCellValueFactory(new PropertyValueFactory<KomandasStatistika, String>("papildlaikaUzvaras"));
        TableColumn papildlaikaZaudejumiColumn = new TableColumn("Papildlaika zaudējumi");
        papildlaikaZaudejumiColumn.setCellValueFactory(new PropertyValueFactory<KomandasStatistika, String>("papildaikaZaudejumi"));
        TableColumn gutieVartiColumn = new TableColumn("Gūtie Vārti");
        gutieVartiColumn.setCellValueFactory(new PropertyValueFactory<KomandasStatistika, String>("gutieVarti"));
        TableColumn zaudetieVartiColumn = new TableColumn("Zaudētie Vārti");
        zaudetieVartiColumn.setCellValueFactory(new PropertyValueFactory<KomandasStatistika, String>("zaudetieVarti"));

        table.setItems(fb.mapKomanduStatistikasToTable());
        table.getColumns().addAll(reitingColumn, nosaukumaColumn, iegutoPunktuColumn, pamatlaikaUzvarasColumn, pamatlaikaZaudejumiColumn,
                papildlaikaUzvarasColumn, papildlaikaZaudejumiColumn, gutieVartiColumn, zaudetieVartiColumn);
        final VBox vbox = new VBox();
        vbox.setSpacing(5);
        vbox.setPadding(new Insets(10, 0, 0, 10));
        vbox.getChildren().addAll(label, table);

        ((Group) scene.getRoot()).getChildren().addAll(vbox);

        stage.setScene(scene);
        stage.show();
    }
}
