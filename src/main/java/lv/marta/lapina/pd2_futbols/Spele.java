/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.marta.lapina.pd2_futbols;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Marta
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Spele {
    //default values are assigned in case a matching field is not found in JSON
    SimpleDateFormat formatter;

    @JsonProperty("Laiks")
    private Date laiks; //TODO: add DateTimeFormatter and make it work

    @JsonProperty("Skatitaji")
    private int skatitaji = 0;
    @JsonProperty("Vieta")
    private String vieta = "";
    @JsonProperty("T")
    private ArrayList<Tiesnesis> tiesnesi; //linijtiesnesi
    @JsonProperty("Komanda")
    private ArrayList<Komanda> komandas;
    @JsonProperty("VT")
    private Tiesnesis vt = null;

    @JsonCreator
    public Spele() {
        formatter =  new SimpleDateFormat("yyyy/MM/dd");
    }


    /**
     * @param aLaiks the laiks to set
     */
    public void setLaiks(String aLaiks) throws ParseException {
        laiks = formatter.parse(aLaiks);
    }

    /**
     * @return the laiks
     */
    public Date getLaiks() {
        return laiks;
    }

    /**
     * @return the skatitaji
     */
    public int getSkatitaji() {
        return skatitaji;
    }

    /**
     * @param skatitaji the skatitaji to set
     */
    public void setSkatitaji(int skatitaji) {
        this.skatitaji = skatitaji;
    }

    /**
     * @return the vieta
     */
    public String getVieta() {
        return vieta;
    }

    /**
     * @param vieta the vieta to set
     */
    public void setVieta(String vieta) {
        this.vieta = vieta;
    }

    /**
     * @return the tiesnesi
     */
    public ArrayList<Tiesnesis> getTiesnesi() {
        return tiesnesi;
    }

    /**
     * @param tiesnesi the tiesnesi to set
     */
    public void setTiesnesi(ArrayList<Tiesnesis> tiesnesi) {
        this.tiesnesi = tiesnesi;
    }

    /**
     * @return the vt
     */
    public Tiesnesis getVt() {
        return vt;
    }

    /**
     * @param vt the vt to set
     */
    public void setVt(Tiesnesis vt) {
        this.vt = vt;
    }

    public ArrayList<Komanda> getKomandas() {
        return komandas;
    }

    public void setKomandas(ArrayList<Komanda> komandas) {
        this.komandas = komandas;
    }

}
