package lv.marta.lapina.pd2_futbols;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Speletaji {
    @JsonProperty("Speletajs")
    private ArrayList<Speletajs> speletaji;

    @JsonCreator
    public Speletaji() {
    }

    public ArrayList<Speletajs> getSpeletaji() {
        return speletaji;
    }

    public void setSpeletaji(ArrayList<Speletajs> speletaji) {
        this.speletaji = speletaji;
    }

    @Override
    public String toString() {
        return "Speletaji{" +
                "speletaji=" + speletaji.toString() +
                '}';
    }
}
