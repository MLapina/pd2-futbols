package lv.marta.lapina.pd2_futbols;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Mainas {
    @JsonProperty("Maina")
    private ArrayList<Maina> maina;

    @JsonCreator
    public Mainas() {
        maina = null;
    }

    public ArrayList<Maina> getMaina() {
        return maina;
    }

    public void setMaina(ArrayList<Maina> maina) {
        this.maina = maina;
    }
}
