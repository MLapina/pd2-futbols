package lv.marta.lapina.pd2_futbols;


public class KomandasStatistika {

    private long reitings;
    private String nosaukums;
    private int iegutiePunkti;
    private int pamatlaikaUzvaras;
    private int pamatlaikaZaudejumi;
    private int papildlaikaUzvaras;
    private int papildaikaZaudejumi;
    private int gutieVarti;
    private int zaudetieVarti;

    public KomandasStatistika() {
    }

    public KomandasStatistika(long reitings, String nosaukums, int iegutiePunkti, int pamatlaikaUzvaras,
                              int pamatlaikaZaudejumi, int papildlaikaUzvaras, int papildaikaZaudejumi,
                              int gutieVarti, int zaudetieVarti) {
        this.reitings = reitings;
        this.nosaukums = nosaukums;
        this.iegutiePunkti = iegutiePunkti;
        this.pamatlaikaUzvaras = pamatlaikaUzvaras;
        this.pamatlaikaZaudejumi = pamatlaikaZaudejumi;
        this.papildlaikaUzvaras = papildlaikaUzvaras;
        this.papildaikaZaudejumi = papildaikaZaudejumi;
        this.gutieVarti = gutieVarti;
        this.zaudetieVarti = zaudetieVarti;
    }

    public long getReitings() {
        return reitings;
    }

    public void setReitings(long reitings) {
        this.reitings = reitings;
    }

    public String getNosaukums() {
        return nosaukums;
    }

    public void setNosaukums(String nosaukums) {
        this.nosaukums = nosaukums;
    }

    public int getIegutiePunkti() {
        return iegutiePunkti;
    }

    public void setIegutiePunkti(int iegutiePunkti) {
        this.iegutiePunkti = iegutiePunkti;
    }

    public int getPamatlaikaUzvaras() {
        return pamatlaikaUzvaras;
    }

    public void setPamatlaikaUzvaras(int pamatlaikaUzvaras) {
        this.pamatlaikaUzvaras = pamatlaikaUzvaras;
    }

    public int getPamatlaikaZaudejumi() {
        return pamatlaikaZaudejumi;
    }

    public void setPamatlaikaZaudejumi(int pamatlaikaZaudejumi) {
        this.pamatlaikaZaudejumi = pamatlaikaZaudejumi;
    }

    public int getPapildlaikaUzvaras() {
        return papildlaikaUzvaras;
    }

    public void setPapildlaikaUzvaras(int papildlaikaUzvaras) {
        this.papildlaikaUzvaras = papildlaikaUzvaras;
    }
    
    public void addPapildlaikaUzvara() {
        this.papildlaikaUzvaras++;
    }

    public int getPapildaikaZaudejumi() {
        return papildaikaZaudejumi;
    }

    public void setPapildaikaZaudejumi(int papildaikaZaudejumi) {
        this.papildaikaZaudejumi = papildaikaZaudejumi;
    }
    
    public void addPapildlaikaZaudejums() {
        this.papildaikaZaudejumi++;
    }

    public int getGutieVarti() {
        return gutieVarti;
    }

    public void setGutieVarti(int gutieVarti) {
        this.gutieVarti = gutieVarti;
    }

    public int getZaudetieVarti() {
        return zaudetieVarti;
    }

    public void setZaudetieVarti(int zaudetieVarti) {
        this.zaudetieVarti = zaudetieVarti;
    }
}
