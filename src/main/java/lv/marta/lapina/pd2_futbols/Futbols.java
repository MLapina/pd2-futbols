/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.marta.lapina.pd2_futbols;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.bson.Document;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.mongodb.client.model.Projections.excludeId;

/**
 * @author Marta
 */
public class Futbols {

    private MongoClient mongoClient;
    private MongoDatabase db;
    private MongoCollection coll;
    private ObjectMapper mapper;
    private ArrayList<Spele> speles;

    public Futbols() {
    }

    public void connectToDB() {

        try {
            // To connect to mongodb server
            mongoClient = new MongoClient("localhost", 27017);

            // Now connect to your databases
            db = getMongoClient().getDatabase("futbols");
            System.out.println("Connect to database successfully");

            coll = getDb().getCollection("futbols");
            System.out.println("Collection futbols selected successfully");
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    //retrns 0 if document is inserted, insert 1 if document already exists in db
    public int postFileToDB(File f) {
        String line = "";
        String jsonString = "";
        try {
            BufferedReader reader = new BufferedReader(new FileReader(f));
            StringBuilder json = new StringBuilder();

            while ((line = reader.readLine()) != null) {
                json.append(line);
            }
            reader.close();
            jsonString = json.toString();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }

        System.out.println("Checking file " + f.getName());

        //check if document for this game already exists
        if (!gameExistsInDb(jsonString, coll)) {
            //System.out.print(jsonString);
            getColl().insertOne(Document.parse(jsonString));
            return 0;
        } else {
            return 1;
        }
    }

    public boolean gameExistsInDb(String s, MongoCollection c) {
        //computeHash(s);
        MongoCursor<Document> cursor = getColl().find().projection(excludeId()).iterator();
        //System.out.println(cursor.next().toJson());
        try {
            while (cursor.hasNext()) {
                if ((cursor.next().toJson().replaceAll("\\s+", "").equals(s.replaceAll("\\s+", ""))) == true) {
                    return true;
                }
            }
        } finally {
            cursor.close();
        }
        return false;

    }

    public ArrayList<Spele> getMatchesFromDb() {
        mapper = new ObjectMapper();
        //sadi panak, ka ari "Spele" lauks tiek deserializets
        mapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);

        Spele karteja = null;
        MongoCursor<Document> cursor = getColl().find().projection(excludeId()).iterator();
        try {
            while (cursor.hasNext()) {
                try {
                    Document next = cursor.next();
//                    System.out.println(next.toJson());
                    karteja = mapper.readValue(next.toJson(), Spele.class);
                } catch (IOException ex) {
                    Logger.getLogger(Futbols.class.getName()).log(Level.INFO, null, ex);
                }
                System.out.println(karteja);
                System.out.println("Laiks: " + karteja.getLaiks());
//                getSpeles().add(karteja);
                System.out.println("All Game: " + karteja.toString());
            }
        } finally {
            cursor.close();
        }
        return null;
    }

    public String computeHash(String s) {
        //nothing yet
        //was thinking of using this to improve duplicate detection at input
        return "";
    }

    /**
     * @return the mongoClient
     */
    public MongoClient getMongoClient() {
        return mongoClient;
    }

    /**
     * @return the db
     */
    public MongoDatabase getDb() {
        return db;
    }

    /**
     * @return the coll
     */
    public MongoCollection getColl() {
        return coll;
    }

    /**
     * @return the speles
     */
    public ArrayList<Spele> getSpeles() {
        return speles;
    }

    /**
     * @param speles the speles to set
     */
    public void setSpeles(ArrayList<Spele> speles) {
        this.speles = speles;
    }

    public ArrayList<Spele> allGames() {
        ArrayList<Spele> allGameList = new ArrayList<>();
        mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
        MongoCursor<Document> cursor = getColl().find().projection(excludeId()).iterator();
        while (cursor.hasNext()) {
            Document next = cursor.next();
            Spele karteja = null;
            try {
                karteja = mapper.readValue(next.toJson(), Spele.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
            allGameList.add(karteja);
        }
        return allGameList;
    }

    public Map<String, ArrayList<Spele>> getApmekletajuStatistika() {
        ArrayList<Spele> speles = allGames();
        Map<String, ArrayList<Spele>> gamesInEachStadium = new HashMap<>();

        for (Spele spele : speles) {
            String vieta = spele.getVieta();
            if (gamesInEachStadium.isEmpty() || !gamesInEachStadium.containsKey(vieta)) {
                ArrayList<Spele> filteredGames = new ArrayList<>();
                for (Spele spele1 : speles) {
                    if (spele1.getVieta().equals(vieta)) {
                        filteredGames.add(spele1);
                    }
                }
                gamesInEachStadium.put(vieta, filteredGames);
            }

        }

        return gamesInEachStadium;
    }

    public ObservableList<KomandasStatistika> mapKomanduStatistikasToTable() {
        ObservableList<KomandasStatistika> data = FXCollections.observableArrayList();
        Map<String, KomandasStatistika> komanduStatistikas = sagatavotStatistiku();
        for (String komanda : komanduStatistikas.keySet()) {
            KomandasStatistika komandasStatistika = komanduStatistikas.get(komanda);
            data.add(komandasStatistika);
        }
        return data;
    }

    public Map<String, KomandasStatistika> sagatavotStatistiku() {
        ArrayList<Spele> visasSpeles = allGames();
        Map<String, KomandasStatistika> komanduStatistikas = new HashMap<>();

        komanduStatistikas = setKomandasNosaukumsUnGutieVarti(visasSpeles, komanduStatistikas);

        for (Spele vienaSpele : visasSpeles) {
            Map<String, KomandasStatistika> punktiParSpeli = skaitaPunktusParSpeli(vienaSpele);
            for (String komandasNosaukums : punktiParSpeli.keySet()) {
                KomandasStatistika komandasStatistika = komanduStatistikas.get(komandasNosaukums);
                //set punkti
                komandasStatistika.setIegutiePunkti(komandasStatistika.getIegutiePunkti() + punktiParSpeli.get(komandasNosaukums).getIegutiePunkti());
                //set papildlaika uzvaras un zaudejumi
                komandasStatistika.setPapildlaikaUzvaras(komandasStatistika.getPapildlaikaUzvaras() + punktiParSpeli.get(komandasNosaukums).getPapildlaikaUzvaras());
                komandasStatistika.setPapildaikaZaudejumi(komandasStatistika.getPapildaikaZaudejumi() + punktiParSpeli.get(komandasNosaukums).getPapildaikaZaudejumi());
                //set pamatlaika uzvaras un zaudejumi
                komandasStatistika.setPamatlaikaUzvaras(komandasStatistika.getPamatlaikaUzvaras() + punktiParSpeli.get(komandasNosaukums).getPamatlaikaUzvaras());
                //komandasStatistika.setPamatlaikaUzvaras(komandasStatistika.getPamatlaikaUzvaras());
                komandasStatistika.setPamatlaikaZaudejumi(komandasStatistika.getPamatlaikaZaudejumi() + punktiParSpeli.get(komandasNosaukums).getPamatlaikaZaudejumi());
                System.out.println("Komanda " + komandasNosaukums + " guvusi punktus " + komandasStatistika.getIegutiePunkti());
                komanduStatistikas.put(komandasStatistika.getNosaukums(), komandasStatistika);
            }
        }

        komanduStatistikas = setZaudetieVarti(visasSpeles, komanduStatistikas);
        return setReitings(komanduStatistikas);
    }

    private Map<String, KomandasStatistika> skaitaPunktusParSpeli(Spele vienaSpele) {
        Map<String, KomandasStatistika> komanduStatistikas = new HashMap<>();
        KomandasStatistika uzvaretajKomanda = new KomandasStatistika();
        KomandasStatistika zaudetajKomanda = new KomandasStatistika();
        final ArrayList<Komanda> komandas = vienaSpele.getKomandas();
        /////// Papildlaiku spēles ////////
        boolean irPapildlaiks = false;
        for (Komanda komanda : komandas) {
            irPapildlaiks = irVartiPapildlaika(komanda);
            if (irPapildlaiks) {
                break;
            }
        }
        if (irPapildlaiks == true) {
            for (Komanda komanda : komandas) {
                if (irVartiPapildlaika(komanda)) {
                    uzvaretajKomanda.setNosaukums(komanda.getNosaukums());
                    uzvaretajKomanda.setPapildlaikaUzvaras(1);
                    uzvaretajKomanda.setIegutiePunkti(3);
                    zaudetajKomanda.setIegutiePunkti(2);
                    zaudetajKomanda.setPapildaikaZaudejumi(1);
                }
                if (zaudetajKomanda.getNosaukums() == null) {
                    zaudetajKomanda.setNosaukums(komanda.getNosaukums());
                }
            }
            komanduStatistikas.put(uzvaretajKomanda.getNosaukums(), uzvaretajKomanda);
            komanduStatistikas.put(zaudetajKomanda.getNosaukums(), zaudetajKomanda);
            return komanduStatistikas;
        }
        /////// Papildlaiku spēle ////////

        /////// Sausās spēle ////////
        if (irPapildlaiks == false || komandas.get(0).getVarti() == null) {
            uzvaretajKomanda.setNosaukums(komandas.get(1).getNosaukums());
            uzvaretajKomanda.setIegutiePunkti(5);
            uzvaretajKomanda.setPamatlaikaUzvaras(1);
            zaudetajKomanda.setNosaukums(komandas.get(0).getNosaukums());
            zaudetajKomanda.setIegutiePunkti(1);
            zaudetajKomanda.setPamatlaikaZaudejumi(1);

            komanduStatistikas.put(uzvaretajKomanda.getNosaukums(), uzvaretajKomanda);
            komanduStatistikas.put(zaudetajKomanda.getNosaukums(), zaudetajKomanda);
            return komanduStatistikas;
        }

        if (irPapildlaiks == false || komandas.get(1).getVarti() == null) {
            uzvaretajKomanda.setNosaukums(komandas.get(0).getNosaukums());
            uzvaretajKomanda.setIegutiePunkti(5);
            uzvaretajKomanda.setPamatlaikaUzvaras(1);
            zaudetajKomanda.setNosaukums(komandas.get(1).getNosaukums());
            zaudetajKomanda.setIegutiePunkti(1);
            zaudetajKomanda.setPamatlaikaZaudejumi(1);

            komanduStatistikas.put(uzvaretajKomanda.getNosaukums(), uzvaretajKomanda);
            komanduStatistikas.put(zaudetajKomanda.getNosaukums(), zaudetajKomanda);
            return komanduStatistikas;
        }
        /////// Sausās spēle ////////

        /////// Normāla spēle ////////
        if (irPapildlaiks == false && komandas.get(0).getVarti() == null && komandas.get(1).getVarti() == null) {
            int vartiPirmaiKomandai;
            int vartiOtraiKomandai;
            vartiPirmaiKomandai = komandas.get(0).getVarti().getVarti().size();
            vartiOtraiKomandai = komandas.get(1).getVarti().getVarti().size();
            if (vartiPirmaiKomandai > vartiOtraiKomandai) {
                uzvaretajKomanda.setNosaukums(komandas.get(0).getNosaukums());
                uzvaretajKomanda.setIegutiePunkti(5);
                uzvaretajKomanda.setPamatlaikaUzvaras(1);
                zaudetajKomanda.setNosaukums(komandas.get(1).getNosaukums());
                zaudetajKomanda.setIegutiePunkti(1);
                zaudetajKomanda.setPamatlaikaZaudejumi(1);
            } else {
                uzvaretajKomanda.setNosaukums(komandas.get(1).getNosaukums());
                uzvaretajKomanda.setIegutiePunkti(5);
                uzvaretajKomanda.setPamatlaikaUzvaras(1);
                zaudetajKomanda.setNosaukums(komandas.get(0).getNosaukums());
                zaudetajKomanda.setIegutiePunkti(1);
                zaudetajKomanda.setPamatlaikaZaudejumi(1);
            }
            /////// Normāla spēle ////////
        }
        komanduStatistikas.put(uzvaretajKomanda.getNosaukums(), uzvaretajKomanda);
        komanduStatistikas.put(zaudetajKomanda.getNosaukums(), zaudetajKomanda);

        return komanduStatistikas;
    }


    private Map<String, KomandasStatistika> setKomandasNosaukumsUnGutieVarti
            (ArrayList<Spele> visasSpeles, Map<String, KomandasStatistika> komanduStatistikas) {
        for (Spele spele : visasSpeles) {
            System.out.println("Spēles rezultāti: ");
            for (Komanda komanda : spele.getKomandas()) {
                if (!komanduStatistikas.containsKey(komanda.getNosaukums())) {
                    KomandasStatistika komandasStatistika = new KomandasStatistika();
                    komandasStatistika.setNosaukums(komanda.getNosaukums());
                    komandasStatistika.setGutieVarti(komanda.getVarti().getVarti().size());
                    komanduStatistikas.put(komanda.getNosaukums(), komandasStatistika);
                    System.out.println("Komanda: " + komanda.getNosaukums());
                    System.out.println("Varti: " + komanda.getVarti().getVarti().size());
                } else {
                    KomandasStatistika komandasStatistika = komanduStatistikas.get(komanda.getNosaukums());
                    if (komanda.getVarti() != null) {
                        komandasStatistika.setGutieVarti(komandasStatistika.getGutieVarti() + komanda.getVarti().getVarti().size());
                        komanduStatistikas.put(komanda.getNosaukums(), komandasStatistika);
                        System.out.println("Komanda: " + komanda.getNosaukums());
                        System.out.println("Varti: " + komanda.getVarti().getVarti().size());
                    }
                }
            }
            System.out.println("Spēles beigas.");
        }
        return komanduStatistikas;
    }

    public boolean irVartiPapildlaika(Komanda komanda) {
        boolean ir = false;
        if (komanda.getVarti() == null) {
            ir = false;
        } else {
            for (VartuGuvums vg : komanda.getVarti().getVarti()) {
                int minutes = Integer.parseInt(vg.getLaiks().split(":")[0]);
                int sekundes = Integer.parseInt(vg.getLaiks().split(":")[1]);
                if ((minutes > 60) && (sekundes > 0)) ir = true;

            }
        }
        return ir;
    }

    //pieskir komandai reitinga poziciju, balstoties uz ieguto punktu skaitu
    public Map<String, KomandasStatistika> setReitings(Map<String, KomandasStatistika> komanduStatistikas) {
        // Convert Map to List
        List<Map.Entry<String, KomandasStatistika>> entryList = new LinkedList<Map.Entry<String, KomandasStatistika>>(komanduStatistikas.entrySet());

        // Sort list with comparator, to compare the Map values
        Collections.sort(entryList, new Comparator<Map.Entry<String, KomandasStatistika>>() {
            public int compare(Map.Entry<String, KomandasStatistika> o1,
                               Map.Entry<String, KomandasStatistika> o2) {
                //o1 lielaks par o2 - atgriez 1, ja vienadi, atgriez 0, ja o1 mazaks par o2, atgriež -1
                return o1.getValue().getIegutiePunkti() < o2.getValue().getIegutiePunkti() ? 1 : o1.getValue().getIegutiePunkti() > o2.getValue().getIegutiePunkti() ? -1 : 0;
            }
        });

        // Convert sorted map back to a Map
        Map<String, KomandasStatistika> sortedkomanduStatistikas = new LinkedHashMap<String, KomandasStatistika>();
        int i = 0; //rating position number
        for (Iterator<Map.Entry<String, KomandasStatistika>> it = entryList.iterator(); it.hasNext(); ) {
            Map.Entry<String, KomandasStatistika> entry = it.next();
            entry.getValue().setReitings(++i);
            sortedkomanduStatistikas.put(entry.getKey(), entry.getValue());
        }
        return sortedkomanduStatistikas;
    }

    public Map<String, KomandasStatistika> setZaudetieVarti(ArrayList<Spele> visasSpeles, Map<String, KomandasStatistika> komanduStatistikas) {

        for (Spele spele : visasSpeles) {
            if (spele.getKomandas().get(0).getVarti() == null && spele.getKomandas().get(1).getVarti() != null && !irVartiPapildlaika(spele.getKomandas().get(1))) {
                KomandasStatistika uzvaretajaStatistika = komanduStatistikas.get(spele.getKomandas().get(1).getNosaukums());
                //uzvaretajaStatistika.setPamatlaikaUzvaras(uzvaretajaStatistika.getPapildlaikaUzvaras() + 1);
                komanduStatistikas.put(uzvaretajaStatistika.getNosaukums(), uzvaretajaStatistika);

                KomandasStatistika zaudetajaStatistika = komanduStatistikas.get(spele.getKomandas().get(0).getNosaukums());
                //zaudetajaStatistika.setPamatlaikaZaudejumi(zaudetajaStatistika.getPamatlaikaZaudejumi() + 1);
                zaudetajaStatistika.setZaudetieVarti(zaudetajaStatistika.getZaudetieVarti() + spele.getKomandas().get(1).getVarti().getVarti().size());
                komanduStatistikas.put(zaudetajaStatistika.getNosaukums(), zaudetajaStatistika);
            }
            if (spele.getKomandas().get(0).getVarti() != null && spele.getKomandas().get(1).getVarti() == null && !irVartiPapildlaika(spele.getKomandas().get(0))) {
                KomandasStatistika uzvaretajaStatistika = komanduStatistikas.get(spele.getKomandas().get(0).getNosaukums());
                //uzvaretajaStatistika.setPamatlaikaUzvaras(uzvaretajaStatistika.getPapildlaikaUzvaras() + 1);
                komanduStatistikas.put(uzvaretajaStatistika.getNosaukums(), uzvaretajaStatistika);

                KomandasStatistika zaudetajaStatistika = komanduStatistikas.get(spele.getKomandas().get(1).getNosaukums());
                //zaudetajaStatistika.setPamatlaikaZaudejumi(zaudetajaStatistika.getPamatlaikaZaudejumi() + 1);
                zaudetajaStatistika.setZaudetieVarti(zaudetajaStatistika.getZaudetieVarti() + spele.getKomandas().get(0).getVarti().getVarti().size());
                komanduStatistikas.put(zaudetajaStatistika.getNosaukums(), zaudetajaStatistika);
            }
        }
        return komanduStatistikas;
    }
}
