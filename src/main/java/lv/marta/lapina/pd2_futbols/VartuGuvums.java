/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.marta.lapina.pd2_futbols;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 *
 * @author Marta
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class VartuGuvums {
    @JsonProperty("Laiks")
    private String laiks;
    @JsonProperty("P")
    private ArrayList<Speletajs> piespeles;
    @JsonProperty("Nr")
    private int vartuGuvejs;
    @JsonProperty("Sitiens")
    private String sitiens;
    
//    public VartuGuvums(String laiksString, ArrayList<Integer> piespeles, int vartuGuvejs, String sitiens){
//        this.laiks = Time.valueOf(laiksString);
//        for (int p : piespeles) this.piespeles.add(new Speletajs("", p, "", "")); //TODO: kaut ka salidzinat ar esosajiem speletajiem, un ja numurs sakrit aizvietot so instanci ar to, kurai defineti visi atributi
//        this.vartuGuvejs = new Speletajs ("", vartuGuvejs, "", ""); //TODO: tas pats, kas augstāk
//        this.sitiens = sitiens;
//    }

    @JsonCreator
    public VartuGuvums() {
        piespeles = null;
    }

    public String getLaiks() {
        return laiks;
    }

    public void setLaiks(String laiks) {
        this.laiks = laiks;
    }

    public ArrayList<Speletajs> getPiespeles() {
        return piespeles;
    }

    public void setPiespeles(ArrayList<Speletajs> piespeles) {
        this.piespeles = piespeles;
    }

    public int getVartuGuvejs() {
        return vartuGuvejs;
    }

    public void setVartuGuvejs(int vartuGuvejs) {
        this.vartuGuvejs = vartuGuvejs;
    }

    public String getSitiens() {
        return sitiens;
    }

    public void setSitiens(String sitiens) {
        this.sitiens = sitiens;
    }

}
