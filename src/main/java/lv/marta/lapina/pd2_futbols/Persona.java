/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.marta.lapina.pd2_futbols;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Marta
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Persona {
    @JsonProperty("Vards")
    private String vards;
    @JsonProperty("Uzvards")
    private String uzvards;

    @JsonCreator
    public Persona() {
    }

    /**
     * @return the vards
     */


    public String getVards() {
        return vards;
    }

    /**
     * @param vards the vards to set
     */
    public void setVards(String vards) {
        this.vards = vards;
    }

    /**
     * @return the uzvards
     */
    public String getUzvards() {
        return uzvards;
    }

    /**
     * @param uzvards the uzvards to set
     */
    public void setUzvards(String uzvards) {
        this.uzvards = uzvards;
    }
    
}