/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.marta.lapina.pd2_futbols;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Marta
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Speletajs extends Persona {

    @JsonProperty("Loma")
    private String loma;
    @JsonProperty("Nr")
    private int numurs;

    @JsonCreator
    public Speletajs() {
    }

    /**
     * @return the loma
     */
    public String getLoma() {
        return loma;
    }

    /**
     * @param loma the loma to set
     */
    public void setLoma(String loma) {
        this.loma = loma;
    }

    /**
     * @return the numurs
     */
    public int getNumurs() {
        return numurs;
    }

    /**
     * @param numurs the numurs to set
     */
    public void setNumurs(int numurs) {
        this.numurs = numurs;
    }

    @Override
    public String toString() {
        return "Speletajs{" +
                "loma='" + loma + '\'' +
                ", numurs=" + numurs +
                '}';
    }
}
