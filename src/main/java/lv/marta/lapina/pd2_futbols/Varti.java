package lv.marta.lapina.pd2_futbols;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Varti {
    @JsonProperty("VG")
    private ArrayList<VartuGuvums> varti;

    @JsonCreator
    public Varti() {
    }

    public ArrayList<VartuGuvums> getVarti() {
        return varti;
    }

    public void setVarti(ArrayList<VartuGuvums> varti) {
        this.varti = varti;
    }

    @Override
    public String toString() {
        return "Varti{" +
                "varti=" + varti +
                '}';
    }
}
