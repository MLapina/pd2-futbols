/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.marta.lapina.pd2_futbols;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Marta
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Maina {

    @JsonProperty("Laiks")
    private String laiks;
    @JsonProperty("Nr1")
    private int kuruNomaina;
    @JsonProperty("Nr2")
    private int kasNakVieta;

    @JsonCreator
    public Maina() {
    }

    public String getLaiks() {
        return laiks;
    }

    public void setLaiks(String laiks) {
        this.laiks = laiks;
    }

    public int getKuruNomaina() {
        return kuruNomaina;
    }

    public void setKuruNomaina(int kuruNomaina) {
        this.kuruNomaina = kuruNomaina;
    }

    public int getKasNakVieta() {
        return kasNakVieta;
    }

    public void setKasNakVieta(int kasNakVieta) {
        this.kasNakVieta = kasNakVieta;
    }
}