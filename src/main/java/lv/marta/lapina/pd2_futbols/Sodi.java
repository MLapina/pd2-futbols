package lv.marta.lapina.pd2_futbols;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Sodi {

    @JsonProperty("Sods")
    private ArrayList<Sods> sodi;

    @JsonCreator
    public Sodi() {
    }

    public ArrayList<Sods> getSodi() {
        return sodi;
    }

    public void setSodi(ArrayList<Sods> sodi) {
        this.sodi = sodi;
    }
}
